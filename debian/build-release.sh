#!/bin/bash

mkdir -p debian/square2/var/lib/square2

chmod 755 -R debian/square2/DEBIAN
chmod 644 debian/square2/DEBIAN/control

./gradlew clean build

G=$(grep "^version =" build.gradle | sed -e "s/version = //g" | sed -e "s/'//g")

echo "found version $G"

rm -f debian/square2/var/lib/square2/*.jar
cp -v build/libs/SQuaRe2-$G.jar debian/square2/var/lib/square2

sed -i debian/square2/DEBIAN/control -e "s/VERSION/$G/g"
sed -i debian/square2/usr/bin/square2 -e "s/VERSION/$G/g"

V=$(grep "Version" debian/square2/DEBIAN/control | sed -e "s/Version: //g")

fakeroot dpkg-deb -b -Zxz debian/square2 debian/square2_$V.deb
