package de.ship.square2.common;

import java.io.Serializable;

import org.springframework.stereotype.Service;

/**
 * 
 * @author henkej
 *
 */
@Service
public class SessionBean implements Serializable {
	private static final long serialVersionUID = 2L;

	private String username;
	private Boolean isAdmin;
	private String logging;

	/**
	 * @return true if the user has admin privileges
	 */
	public Boolean getIsAdmin() {
		return isAdmin;
	}

	/**
	 * set the field isAdmin
	 * 
	 * @param isAdmin the new value
	 */
	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	/**
	 * @return true if username is not null, false otherwise
	 */
	public Boolean hasUsername() {
		return username != null;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the logging
	 */
	public String getLogging() {
		return logging;
	}

	/**
	 * @param logging the logging to set
	 */
	public void setLogging(String logging) {
		this.logging = logging;
	}
}
