package de.ship.square2.common;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class LambdaResultWrapper implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer integer;
	private String string;

	/**
	 * add the value of string to the internal one
	 * 
	 * @param string    the string
	 * @param linebreak if true, use the linebreak as a separator
	 */
	public void add(String string, boolean linebreak) {
		this.string = this.string == null ? string : string.concat(linebreak ? "\n" : "").concat(string);
	}

	/**
	 * add the value of integer to the internal one
	 * 
	 * @param integer the integer
	 */
	public void add(Integer integer) {
		this.integer = this.integer == null ? integer : this.integer + integer;
	}

	/**
	 * @return the integer
	 */
	public Integer getInteger() {
		return integer;
	}

	/**
	 * @param integer the integer to set
	 */
	public void setInteger(Integer integer) {
		this.integer = integer;
	}

	/**
	 * @return the string
	 */
	public String getString() {
		return string;
	}

	/**
	 * @param string the string to set
	 */
	public void setString(String string) {
		this.string = string;
	}
}
