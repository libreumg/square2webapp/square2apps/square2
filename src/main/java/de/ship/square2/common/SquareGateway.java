package de.ship.square2.common;

import static de.ship.dbppsquare.square.Tables.V_VERSION;

import org.jooq.DSLContext;

/**
 * 
 * @author henkej
 *
 */
public abstract class SquareGateway {

	/**
	 * get the database version
	 * 
	 * @param jooq the DSL context
	 * @return the version
	 */
	public Integer getVersion(DSLContext jooq) {
		return jooq.selectFrom(V_VERSION).fetchOne(V_VERSION.DB_VERSION);
	}
}
