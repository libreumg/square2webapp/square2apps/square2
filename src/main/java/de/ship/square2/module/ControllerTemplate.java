package de.ship.square2.module;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;

import ch.qos.logback.classic.Level;
import de.ship.square2.common.SessionBean;

/**
 * 
 * @author henkej
 *
 */
public abstract class ControllerTemplate {
	private static final String APPID = "SQuaRe²";
	
	@Value("${keycloak.auth-server-url}")
	private String keycloakAuthServerUrl;
	
	@Value("${keycloak.realm}")
	private String realm;

	/**
	 * setup the user for the session.
	 * 
	 * @param request      the request
	 * @param indexService the servicee
	 * @param sessionBean  the bean
	 */
	public void setupUserSession(HttpServletRequest request, ISessionService indexService, SessionBean sessionBean) {
		String username = indexService.getUsername(request);
		sessionBean.setUsername(username);
		sessionBean.setIsAdmin(indexService.getRoles(request).contains("admin"));
	}

	/**
	 * setup the session
	 * 
	 * @param model
	 * @param request
	 * @param sessionBean
	 * @param sessionService
	 */
	public void setupSession(Model model, HttpServletRequest request, SessionBean sessionBean, ISessionService sessionService) {
		sessionBean.setUsername(sessionService.getUsername(request));
		sessionBean.setIsAdmin(sessionService.getRoles(request).contains("admin"));
		ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getILoggerFactory()
				.getLogger("de.ship.square2");
		sessionBean
				.setLogging(logger == null || logger.getLevel() == null ? Level.INFO.levelStr : logger.getLevel().levelStr);
		Set<String> roles = sessionService.getRoles(request);
		model.addAttribute("menuitems", sessionService.getMenu(roles, LocaleContextHolder.getLocale().getLanguage()));
		model.addAttribute("roles", roles);
		model.addAttribute("appid", APPID);
		StringBuilder buf = new StringBuilder();
		buf.append(keycloakAuthServerUrl);
		buf.append("/realms/").append(realm).append("/account/");
		model.addAttribute("keycloakuserclienturl", buf.toString());
	}
}
