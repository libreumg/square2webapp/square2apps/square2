package de.ship.square2.module;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import de.ship.square2.module.menu.MenuitemBean;

/**
 * 
 * @author henkej
 *
 */
public interface ISessionService {

	/**
	 * get the username from the keycloak session in the request
	 * 
	 * @param request the request
	 * @return the username
	 */
	public String getUsername(HttpServletRequest request);

	/**
	 * get the roles of the keycloak session in the request
	 * 
	 * @param request the request
	 * @return the roles; at least an empty list
	 */
	public Set<String> getRoles(HttpServletRequest request);

	/**
	 * get the menu from the main application. Find it by calling SQuaRe2/menu which
	 * returns the valid menu entries as a json string.
	 * 
	 * @param roles the roles of this user to filter the menus by
	 * @param locale the locale for the translations of the menu name and description
	 * 
	 * @return the menu items
	 */
	public List<MenuitemBean> getMenu(Set<String> roles, String locale);

}
