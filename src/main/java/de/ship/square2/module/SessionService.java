package de.ship.square2.module;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.IDToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import de.ship.square2.module.menu.MenuitemBean;
import de.ship.square2.module.menu.MenuitemBeanConverter;

/**
 * 
 * @author henkej
 *
 */
@Service
public class SessionService implements ISessionService {
	private static final Logger LOGGER = LogManager.getLogger(SessionService.class);

	@Value("${reference.application.menu}")
	private String menuurl;

	@Autowired
	private MenuitemBeanConverter menuitemBeanConverter;
	
	/**
	 * Get the id token from the request on keycloak connections
	 *
	 * @param request the request
	 * @return the id token of the keycloak connection
	 */
	public IDToken getIdTokenFromPrincipal(HttpServletRequest request) {
		KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request.getUserPrincipal();
		return principal == null ? null : principal.getAccount().getKeycloakSecurityContext().getIdToken();
	}

	@Override
	public String getUsername(HttpServletRequest request) {
		IDToken idToken = getIdTokenFromPrincipal(request);
		return idToken == null ? null : idToken.getPreferredUsername();
	}

	@Override
	public Set<String> getRoles(HttpServletRequest request) {
		KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request.getUserPrincipal();
		Set<String> roles = new HashSet<>();
		if (principal == null) {
			return new HashSet<>(); // maybe not valid login or the keycloak server is down
		}
		for (GrantedAuthority granted : principal.getAuthorities()) {
			String r = granted.getAuthority();
			// remove possible keycloak prefix ROLE_
			roles.add(r.startsWith("ROLE_") ? r.replace("ROLE_", "") : r);
		}
		return roles;
	}

	@Override
	public List<MenuitemBean> getMenu(Set<String> roles, String locale) {
		StringBuilder url = new StringBuilder(menuurl);
		url.append("/").append(locale);
		RestTemplate rest = new RestTemplate();
		rest.getMessageConverters().add(menuitemBeanConverter);
		LOGGER.debug("requesting {}", url.toString());
		ResponseEntity<MenuitemBean[]> response = rest.getForEntity(url.toString(), MenuitemBean[].class);
		List<MenuitemBean> list = new ArrayList<>();
		for (MenuitemBean bean : response.getBody()) {
			if (bean.hasAnyRole(roles)) {
				list.add(bean);
			} else {
				LOGGER.debug("roles {} not found in {} roles: {}", roles, bean.getLabel(), bean.getRoles());
			}
		}
		return list;
	}
}
