package de.ship.square2.module.menu;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author henkej
 *
 */
@Repository
public class UrlRepository {
	private static final Logger LOGGER = LogManager.getLogger(UrlRepository.class);

	/**
	 * check, if the server on url is available
	 * 
	 * @param url the url
	 * @return true or false
	 * @throws IOException on errors
	 */
	public Boolean isUrlAvailable(String url) {
		// TODO: let this be done by ajax calls to reduce load time, but find a solution for ajax CORS first
		HttpURLConnection connection = null;
		try {
			URL remote = new URL(url);
			connection = (HttpURLConnection) remote.openConnection();
			connection.setConnectTimeout(200); // 200 ms is enough, we have 7 calls
			connection.setReadTimeout(500);
			connection.setRequestMethod("HEAD");
			connection.setRequestProperty("Connection", "close"); // omit to spam the log file
			int responseCode = connection.getResponseCode();
			connection.disconnect();
			return (200 <= responseCode && responseCode <= 399);
		} catch (IOException e) {
			LOGGER.debug(e); // apps might be down, omit logging this in the error loglevel
			return false;
		} finally {
			connection.disconnect();
			connection = null;
		}
	}
}
