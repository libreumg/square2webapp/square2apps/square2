package de.ship.square2.module.admin.model;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import de.ship.dbppsquare.appconfig.tables.records.TMenuitemRecord;

/**
 * 
 * @author henkej
 *
 */
@Mapper
public interface MenuItemBeanMapper {
	MenuItemBeanMapper INSTANCE = Mappers.getMapper(MenuItemBeanMapper.class);
	
	@InheritInverseConfiguration
	MenuItemBean fromRecord(TMenuitemRecord bean);
	
	default List<MenuItemBean> fromList(List<TMenuitemRecord> list) {
		List<MenuItemBean> result = new ArrayList<>(list != null ? list.size() : 0);
		for (TMenuitemRecord bean : list) {
			result.add(MenuItemBeanMapper.INSTANCE.fromRecord(bean));
		}
		return result;
	}
}
