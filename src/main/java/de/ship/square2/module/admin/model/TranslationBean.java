package de.ship.square2.module.admin.model;

import java.io.Serializable;

import de.ship.dbppsquare.appconfig.enums.EnumCategory;
import de.ship.dbppsquare.appconfig.enums.EnumLocale;

/**
 * 
 * @author henkej
 *
 */
public class TranslationBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer pk;
	private Integer fkMenuItem;
	private EnumLocale locale;
	private EnumCategory category;
	private String value;
	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}
	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}
	/**
	 * @return the fkMenuItem
	 */
	public Integer getFkMenuItem() {
		return fkMenuItem;
	}
	/**
	 * @param fkMenuItem the fkMenuItem to set
	 */
	public void setFkMenuItem(Integer fkMenuItem) {
		this.fkMenuItem = fkMenuItem;
	}
	/**
	 * @return the locale
	 */
	public EnumLocale getLocale() {
		return locale;
	}
	/**
	 * @param locale the locale to set
	 */
	public void setLocale(EnumLocale locale) {
		this.locale = locale;
	}
	/**
	 * @return the category
	 */
	public EnumCategory getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(EnumCategory category) {
		this.category = category;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
