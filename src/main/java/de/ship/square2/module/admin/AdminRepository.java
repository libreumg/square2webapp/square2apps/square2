package de.ship.square2.module.admin;

import static de.ship.dbppsquare.appconfig.Tables.T_MENUITEM;
import static de.ship.dbppsquare.appconfig.Tables.T_MENUROLE;
import static de.ship.dbppsquare.appconfig.Tables.T_TRANSLATION;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertOnDuplicateSetMoreStep;
import org.jooq.InsertReturningStep;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.jooq.SelectJoinStep;
import org.jooq.SelectWhereStep;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.ship.dbppsquare.appconfig.enums.EnumCategory;
import de.ship.dbppsquare.appconfig.enums.EnumLocale;
import de.ship.dbppsquare.appconfig.enums.EnumRole;
import de.ship.dbppsquare.appconfig.tables.records.TMenuitemRecord;
import de.ship.dbppsquare.appconfig.tables.records.TMenuroleRecord;
import de.ship.dbppsquare.appconfig.tables.records.TTranslationRecord;
import de.ship.square2.common.LambdaResultWrapper;
import de.ship.square2.common.SquareGateway;

/**
 * 
 * @author henkej
 *
 */
@Repository
public class AdminRepository extends SquareGateway {
	private final static Logger LOGGER = LogManager.getLogger(AdminRepository.class);

	private final static Integer EXPECTED_VERSION = 77;

	private final DSLContext jooq;

	public AdminRepository(@Autowired DSLContext jooq) {
		this.jooq = jooq;
		Integer version = super.getVersion(jooq);
		if (version < EXPECTED_VERSION) {
			throw new DataAccessException(
					String.format("Database version %d is too low, need at least %d", version, EXPECTED_VERSION));
		}
	}

	/**
	 * get all menu item beans from the database
	 * 
	 * @return a list
	 */
	public List<TMenuitemRecord> getAllMenuItems() {
		SelectWhereStep<TMenuitemRecord> sql = jooq.selectFrom(T_MENUITEM);
		LOGGER.debug(sql.toString());
		return new ArrayList<>(sql.fetch());
	}

	/**
	 * get all translation records from the database
	 * 
	 * @param fkMenuitem the pk of the corresponding menu item
	 * 
	 * @return a list
	 */
	public List<TTranslationRecord> getAllTranslations(Integer fkMenuitem) {
		SelectConditionStep<TTranslationRecord> sql = jooq.selectFrom(T_TRANSLATION)
				.where(T_TRANSLATION.FK_MENUITEM.eq(fkMenuitem));
		LOGGER.debug(sql.toString());
		return new ArrayList<>(sql.fetch());
	}

	/**
	 * get all possible menu roles
	 * 
	 * @return the list of available menu roles
	 */
	public List<String> getAllMenuRoles() {
		SelectJoinStep<Record1<EnumRole>> sql = jooq.selectDistinct(T_MENUROLE.ROLENAME).from(T_MENUROLE);
		LOGGER.debug(sql.toString());
		List<String> list = new ArrayList<>();
		for (EnumRole r : sql.fetch(T_MENUROLE.ROLENAME)) {
			if (r != null) {
				list.add(r.getLiteral());
			}
		}
		return list;
	}

	/**
	 * get all menu role records from the database
	 * 
	 * @param fkMenuitem the pk of the corresponding menu item
	 * 
	 * @return a list
	 */
	public List<String> getAllMenuRoles(Integer fkMenuitem) {
		SelectConditionStep<Record1<EnumRole>> sql = jooq
		// @formatter:off
			.select(T_MENUROLE.ROLENAME)
			.from(T_MENUROLE)
			.where(T_MENUROLE.FK_MENUITEM.eq(fkMenuitem));
		// @formatter:on
		LOGGER.debug(sql.toString());
		List<String> list = new ArrayList<>();
		for (EnumRole r : sql.fetch(T_MENUROLE.ROLENAME)) {
			if (r != null) {
				list.add(r.getLiteral());
			}
		}
		return list;
	}

	/**
	 * get the record of pk
	 * 
	 * @param pk the pk
	 * @return the record or null
	 */
	public TMenuitemRecord getMenuItem(Integer pk) {
		SelectConditionStep<TMenuitemRecord> sql = jooq.selectFrom(T_MENUITEM).where(T_MENUITEM.PK.eq(pk));
		LOGGER.debug(sql.toString());
		return sql.fetchOne();
	}

	/**
	 * update the URL and the orderNr of the element with pk
	 * 
	 * @param pk      the ID of the entry
	 * @param url     the URL
	 * @param orderNr the order number
	 * @return number of affected database rows; should be 1
	 */
	public Integer updateMenuItem(Integer pk, String url, Integer orderNr) {
		UpdateConditionStep<TMenuitemRecord> sql = jooq
		// @formatter:off
			.update(T_MENUITEM)
			.set(T_MENUITEM.URL, url)
			.set(T_MENUITEM.ORDER_NR, orderNr)
			.where(T_MENUITEM.PK.eq(pk));
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}

	/**
	 * update the translation for the referenced element
	 * 
	 * @param pk    the ID of the translation
	 * @param value the new value
	 * @return number of affected database rows; should be 1
	 */
	public Integer updateTranslation(Integer pk, String value) {
		UpdateConditionStep<TTranslationRecord> sql = jooq
		// @formatter:off
			.update(T_TRANSLATION)
			.set(T_TRANSLATION.VALUE, value)
			.where(T_TRANSLATION.PK.eq(pk));
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}

	/**
	 * add a new translation for the menu item referenced by fkMenuitem
	 * 
	 * @param fkMenuitem the ID of the menu item
	 * @param locale     the locale
	 * @param category   the category
	 * @param value      the new value
	 * @return number of affected database rows; should be 1
	 */
	public Integer addTranslation(Integer fkMenuitem, EnumLocale locale, EnumCategory category, String value) {
		InsertOnDuplicateSetMoreStep<TTranslationRecord> sql = jooq
		// @formatter:off
			.insertInto(T_TRANSLATION,
					        T_TRANSLATION.FK_MENUITEM,
					        T_TRANSLATION.LOCALE,
					        T_TRANSLATION.CATEGORY,
					        T_TRANSLATION.VALUE)
			.values(fkMenuitem, locale, category, value)
			.onConflict(T_TRANSLATION.FK_MENUITEM, T_TRANSLATION.LOCALE, T_TRANSLATION.CATEGORY)
			.doUpdate()
			.set(T_TRANSLATION.VALUE, value);
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}

	/**
	 * remove the translation referenced by pk
	 * 
	 * @param pk the ID of the translation
	 * @return number of affected database rows; should be 1
	 */
	public Integer removeTranslation(Integer pk) {
		DeleteConditionStep<TTranslationRecord> sql = jooq
		// @formatter:off
			.deleteFrom(T_TRANSLATION)
			.where(T_TRANSLATION.PK.eq(pk));
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}

	/**
	 * update the roles table for the list of valid roles
	 * 
	 * @param fkMenuitem the ID of the menu item
	 * @param roles      the roles that are valid for this menu item
	 * @return number of affected database rows; a mix of insert / deletes, may be 0
	 */
	public Integer updateRoles(Integer fkMenuitem, List<String> roles) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		jooq.transaction(t -> {
			DeleteConditionStep<TMenuroleRecord> sql = DSL.using(t)
			// @formatter:off
				.deleteFrom(T_MENUROLE)
				.where(T_MENUROLE.FK_MENUITEM.eq(fkMenuitem))
				.and(T_MENUROLE.ROLENAME.notIn(roles));
			// @formatter:on
			LOGGER.debug(sql.toString());
			lrw.setInteger(sql.execute());

			for (String role : roles) {
				InsertReturningStep<TMenuroleRecord> sql2 = DSL.using(t)
				// @formatter:off
					.insertInto(T_MENUROLE,
							        T_MENUROLE.FK_MENUITEM,
							        T_MENUROLE.ROLENAME)
					.values(fkMenuitem, role == null ? null : EnumRole.valueOf(role))
					.onConflict(T_MENUROLE.FK_MENUITEM, T_MENUROLE.ROLENAME)
					.doNothing();
				// @formatter:on
				LOGGER.debug(sql2.toString());
				lrw.add(sql2.execute());
			}
		});
		return lrw.getInteger();
	}

	/**
	 * add the translation of the name for the locale l
	 * 
	 * @param pk the ID of the menu item
	 * @param locale the locale
	 * @param category the category
	 */
	public void addTranslaction(Integer pk, EnumLocale locale, EnumCategory category) {
		InsertReturningStep<TTranslationRecord> sql = jooq
		// @formatter:off
			.insertInto(T_TRANSLATION,
					        T_TRANSLATION.FK_MENUITEM,
					        T_TRANSLATION.CATEGORY,
					        T_TRANSLATION.LOCALE,
					        T_TRANSLATION.VALUE)
			.values(pk, category, locale, "")
			.onConflict(T_TRANSLATION.FK_MENUITEM, T_TRANSLATION.CATEGORY, T_TRANSLATION.LOCALE)
			.doNothing();
		// @formatter:on
		LOGGER.trace(sql.toString());
		sql.execute();
	}
}
