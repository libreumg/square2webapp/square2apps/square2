package de.ship.square2.module.admin;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import de.ship.square2.common.SessionBean;
import de.ship.square2.module.ControllerTemplate;
import de.ship.square2.module.ISessionService;
import de.ship.square2.module.admin.model.MenuItemBean;

/**
 * 
 * @author henkej
 *
 */
@Controller
public class AdminController extends ControllerTemplate {
	private static final Logger LOGGER = LogManager.getLogger(AdminController.class);

	@Autowired
	private ISessionService sessionService;

	@Autowired
	private SessionBean sessionBean;
	
	@Autowired
	private AdminService adminService;

	@GetMapping("/admin")
	public String admin(Model model, HttpServletRequest request) {
		setupSession(model, request, sessionBean, sessionService);
		return "admin/main";
	}
	
	@GetMapping("/appconfig")
	public String appconfig(Model model, HttpServletRequest request) {
		setupSession(model, request, sessionBean, sessionService);
		model.addAttribute("menuitems", adminService.getAllMenuItems());
		return "admin/appconfig";
	}

	@GetMapping("/menuitem/{pk}")
	public String menuitem(Model model, HttpServletRequest request, @PathVariable Integer pk) {
		setupSession(model, request, sessionBean, sessionService);
		model.addAttribute("menuitem", adminService.getMenuItem(pk));
		model.addAttribute("menuroles", adminService.getAllMenuRoles());
		return "admin/menuitem";
	}
	
	@PostMapping("/menuitem/update")
	public String update(@ModelAttribute MenuItemBean menuitem, Model model, HttpServletRequest request) {
		if (menuitem != null) {
			adminService.updateMenuItem(menuitem);
		} else {
			LOGGER.warn("updating null is not possible");
		}
		return appconfig(model, request);
	}
	
	@GetMapping("/menuitem/{pk}/addname/{l}")
	public String addName(@PathVariable Integer pk, @PathVariable String l) {
		adminService.addName(pk, l);
		return "redirect:/menuitem/" + pk;
	}
	
	@GetMapping("/menuitem/{pk}/adddescription/{l}")
	public String addDescription(@PathVariable Integer pk, @PathVariable String l) {
		adminService.addDescription(pk, l);
		return "redirect:/menuitem/" + pk;
	}
}
