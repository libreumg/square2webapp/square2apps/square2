package de.ship.square2.module.admin.model;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import de.ship.dbppsquare.appconfig.tables.records.TTranslationRecord;

/**
 * 
 * @author henkej
 *
 */
@Mapper
public interface TranslationBeanMapper {
	TranslationBeanMapper INSTANCE = Mappers.getMapper(TranslationBeanMapper.class);
	
	@InheritInverseConfiguration
	TranslationBean fromRecord(TTranslationRecord bean);
	
	default List<TranslationBean> fromList(List<TTranslationRecord> list) {
		List<TranslationBean> result = new ArrayList<>(list != null ? list.size() : 0);
		for (TTranslationRecord bean : list) {
			result.add(TranslationBeanMapper.INSTANCE.fromRecord(bean));
		}
		return result;
	}
}
