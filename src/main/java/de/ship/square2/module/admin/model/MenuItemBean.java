package de.ship.square2.module.admin.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.ship.dbppsquare.appconfig.enums.EnumCategory;
import de.ship.dbppsquare.appconfig.enums.EnumLocale;

/**
 * 
 * @author henkej
 *
 */
public class MenuItemBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String id;
	private String url;
	private Integer orderNr;
	private List<TranslationBean> translations;
	private List<String> roles;

	public MenuItemBean() {
		translations = new ArrayList<>();
		roles = new ArrayList<>();
	}

	/**
	 * check if for locale the name field is missing
	 * 
	 * @param locale the locale
	 * @return true or false
	 */
	public Boolean missingName(String locale) {
		Boolean missing = true;
		for (TranslationBean bean : translations) {
			if (bean.getCategory().equals(EnumCategory.name)) {
				if (bean.getLocale().equals(EnumLocale.lookupLiteral(locale))) {
					missing = false;
				}
			}
		}
		return missing;
	}

	/**
	 * check if for locale the description field is missing
	 * 
	 * @param locale the locale
	 * @return true or false
	 */
	public Boolean missingDescription(String locale) {
		Boolean missing = true;
		for (TranslationBean bean : translations) {
			if (bean.getCategory().equals(EnumCategory.description)) {
				if (bean.getLocale().equals(EnumLocale.lookupLiteral(locale))) {
					missing = false;
				}
			}
		}
		return missing;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the orderNr
	 */
	public Integer getOrderNr() {
		return orderNr;
	}

	/**
	 * @param orderNr the orderNr to set
	 */
	public void setOrderNr(Integer orderNr) {
		this.orderNr = orderNr;
	}

	/**
	 * @return the translations
	 */
	public List<TranslationBean> getTranslations() {
		return translations;
	}

	/**
	 * @return the roles
	 */
	public List<String> getRoles() {
		return roles;
	}

	public void setTranslations(List<TranslationBean> list) {
		translations.clear();
		translations.addAll(list);
	}

	public void setRoles(List<String> list) {
		roles.clear();
		roles.addAll(list);
	}
}
