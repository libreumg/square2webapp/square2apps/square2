package de.ship.square2.module.admin;

import java.util.List;

import org.jooq.exception.DataAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.ship.dbppsquare.appconfig.enums.EnumCategory;
import de.ship.dbppsquare.appconfig.enums.EnumLocale;
import de.ship.square2.module.admin.model.MenuItemBean;
import de.ship.square2.module.admin.model.MenuItemBeanMapper;
import de.ship.square2.module.admin.model.TranslationBean;
import de.ship.square2.module.admin.model.TranslationBeanMapper;

/**
 * 
 * @author henkej
 *
 */
@Service
public class AdminService {

	@Autowired
	private AdminRepository gateway;

	/**
	 * get all menu items
	 * 
	 * @return the list of menu items; an empty list at least
	 */
	public List<MenuItemBean> getAllMenuItems() {
		return MenuItemBeanMapper.INSTANCE.fromList(gateway.getAllMenuItems());
	}
	
	/**
	 * get all menu roles
	 * 
	 * @return the list of menu roles; an empty list at least
	 */
	public List<String> getAllMenuRoles() {
		return gateway.getAllMenuRoles();
	}

	/**
	 * get the menu item of pk
	 * 
	 * @param pk the pk of the item
	 * @return the record if found, null otherwise
	 */
	public MenuItemBean getMenuItem(Integer pk) {
		MenuItemBean bean = MenuItemBeanMapper.INSTANCE.fromRecord(gateway.getMenuItem(pk));
		bean.setTranslations(TranslationBeanMapper.INSTANCE.fromList(gateway.getAllTranslations(pk)));
		bean.setRoles(gateway.getAllMenuRoles(pk));
		return bean;
	}

	/**
	 * update the menu item
	 * 
	 * @param menuitem the menu item
	 * @return the number of affected database rows
	 */
	public Integer updateMenuItem(MenuItemBean menuitem) {
		Integer affected = gateway.updateRoles(menuitem.getPk(), menuitem.getRoles());
		for (TranslationBean bean : menuitem.getTranslations()) {
			affected += gateway.updateTranslation(bean.getPk(), bean.getValue());
		}
		affected += gateway.updateMenuItem(menuitem.getPk(), menuitem.getUrl(), menuitem.getOrderNr());
		return affected;
	}

	/**
	 * add the translation of the name for the locale l
	 * 
	 * @param pk the ID of the menu item
	 * @param l the locale
	 */
	public void addName(Integer pk, String l) {
		EnumLocale locale = EnumLocale.lookupLiteral(l);
		if (locale != null) {
			gateway.addTranslaction(pk, locale, EnumCategory.name);
		} else {
			throw new DataAccessException(String.format("locale %s is not yet supported by this database", l));
		}
	}

	/**
	 * add the translation of the description for the locale l
	 * 
	 * @param pk the ID of the menu item
	 * @param l the locale
	 */
	public void addDescription(Integer pk, String l) {
		EnumLocale locale = EnumLocale.lookupLiteral(l);
		if (locale != null) {
			gateway.addTranslaction(pk, locale, EnumCategory.description);
		} else {
			throw new DataAccessException(String.format("locale %s is not yet supported by this database", l));
		}
	}
}
