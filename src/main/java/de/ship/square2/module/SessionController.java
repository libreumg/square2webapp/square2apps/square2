package de.ship.square2.module;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ch.qos.logback.classic.Level;
import de.ship.square2.common.SessionBean;

/**
 * 
 * @author henkej
 *
 */
@Controller
public class SessionController extends ControllerTemplate {
	private static final Logger LOGGER = LogManager.getLogger(SessionController.class);

	@Autowired
	private ISessionService sessionService;

	@Autowired
	private SessionBean sessionBean;

	@GetMapping("/logout")
	public String logout(HttpServletRequest request) throws ServletException {
		request.logout();
		return "redirect:/";
	}

	@GetMapping("/")
	public String homePage(Model model, HttpServletRequest request) {
		return "index";
	}

	@GetMapping("/impressum")
	public String impressum(Model model, HttpServletRequest request) {
		setupSession(model, request, sessionBean, sessionService);
		return "impressum";
	}

	@GetMapping("/welcome")
	public String welcome(Model model, HttpServletRequest request) {
		setupSession(model, request, sessionBean, sessionService);
		return "welcome";
	}

	@RequestMapping("/logging")
	public String changeLogLevel(HttpServletRequest request, Model model, @RequestParam String level) {
		ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getILoggerFactory()
				.getLogger("de.ship.square2");
		if (logger != null) {
			logger.setLevel(Level.toLevel(level));
			LOGGER.info(String.format("Changed logger to level %s", level));
			setupSession(model, request, sessionBean, sessionService);
			return "logging";
		} else {
			LOGGER.error(String.format("logger not found, cannot set it to level %s", level));
			return "error";
		}
	}

	@RequestMapping("/notyetimplemented/login")
	public String notyetimplemented(Model model, HttpServletRequest request) {
		setupSession(model, request, sessionBean, sessionService);
		return "notyetimplemented";
	}
}
