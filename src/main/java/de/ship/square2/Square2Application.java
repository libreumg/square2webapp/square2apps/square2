package de.ship.square2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author henkej
 *
 */
@SpringBootApplication
public class Square2Application{

	public static void main(String[] args) {
		SpringApplication.run(Square2Application.class, args);
	}

}
